import 'package:flutter/material.dart';

import '../models/people.dart';

typedef OnSaveCallback = Function(String name);

class AddEditPeoplePage extends StatefulWidget {
  final bool isEditing;
  final OnSaveCallback onSave;
  final People? people;

  AddEditPeoplePage({
    required this.onSave,
    required this.isEditing,
    this.people,
  });

  static Route route(
      {required OnSaveCallback onSave,
      required bool isEditing,
      People? people}) {
    return MaterialPageRoute<void>(
        builder: (_) => AddEditPeoplePage(
              onSave: onSave,
              isEditing: isEditing,
              people: people,
            ));
  }

  @override
  _AddEditPeoplePageState createState() => _AddEditPeoplePageState();
}

class _AddEditPeoplePageState extends State<AddEditPeoplePage> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late String _name;

  bool get isEditing => widget.isEditing;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${isEditing ? "Edit" : "Add"} People",
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: [
              TextFormField(
                initialValue: isEditing ? widget.people?.name : '',
                autofocus: !isEditing,
                style: textTheme.headline5,
                decoration: const InputDecoration(
                  hintText: "Enter the name",
                ),
                validator: (val) {
                  return val!.trim().isEmpty ? "Name cannot be empty" : null;
                },
                onSaved: (value) => _name = value!,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: isEditing ? 'Update' : 'Save',
        child: Icon(isEditing ? Icons.check : Icons.add),
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
            if (isEditing) {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('Confirmation'),
                    content: const Text('Update this data?'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          widget.onSave(_name);
                        },
                        child: const Text('Yes'),
                      ),
                      TextButton(
                        onPressed: () => Navigator.pop(context),
                        child: const Text('No'),
                      ),
                    ],
                  );
                },
              );
            } else {
              widget.onSave(_name);
              Navigator.pop(context);
            }
          }
        },
      ),
    );
  }
}
