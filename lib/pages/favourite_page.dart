import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/authentication/authentication_bloc.dart';
import '../blocs/people/people_bloc.dart';
import 'add_edit_people_page.dart';

class FavouritePage extends StatelessWidget {
  const FavouritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PeopleBloc, PeopleState>(
      builder: (context, state) {
        if (state is PeopleLoadInProgress) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is PeopleLoadFailure) {
          return RefreshIndicator(
            onRefresh: () async {
              context.read<PeopleBloc>().add(FavouritePeopleLoaded(
                  BlocProvider.of<AuthenticationBloc>(context).state.user.id ??
                      0));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Center(
                  child: Text('Failed to fetch data. Please pull to refresh'),
                ),
                ElevatedButton(
                  onPressed: () => context.read<PeopleBloc>().add(
                      FavouritePeopleLoaded(
                          BlocProvider.of<AuthenticationBloc>(context)
                                  .state
                                  .user
                                  .id ??
                              0)),
                  child: const Text('Refresh'),
                )
              ],
            ),
          );
        }
        if (state is PeopleLoadSuccess) {
          return RefreshIndicator(
            onRefresh: () async {
              context.read<PeopleBloc>().add(FavouritePeopleLoaded(
                  BlocProvider.of<AuthenticationBloc>(context).state.user.id ??
                      0));
            },
            child: ListView(
              children: state.peoples
                  .map(
                    (people) => Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16.0,
                      ),
                      child: Row(
                        children: [
                          Expanded(child: Text(people.name)),
                          IconButton(
                            onPressed: () => context
                                .read<PeopleBloc>()
                                .add(PeopleFavouriteToggled(people)),
                            icon: Icon(
                              Icons.star,
                              color: people.isFavourite == 1
                                  ? Colors.orange
                                  : Colors.grey,
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                AddEditPeoplePage.route(
                                  onSave: (String name) {
                                    BlocProvider.of<PeopleBloc>(context).add(
                                      PeopleUpdated(
                                        people.copyWith(
                                          name: name,
                                        ),
                                      ),
                                    );
                                  },
                                  isEditing: true,
                                  people: people,
                                ),
                              );
                            },
                            icon: const Icon(
                              Icons.edit,
                              color: Colors.blue,
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: const Text('Confirmation'),
                                    content: const Text('Delete this data?'),
                                    actions: [
                                      TextButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                          context
                                              .read<PeopleBloc>()
                                              .add(PeopleDeleted(people));
                                        },
                                        child: const Text('Yes'),
                                      ),
                                      TextButton(
                                        onPressed: () => Navigator.pop(context),
                                        child: const Text('No'),
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                            icon: const Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                  .toList(),
            ),
          );
        }
        return Container();
      },
    );
  }
}
