import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:people_app/widgets/people_action_buttons.dart';

import '../blocs/authentication/authentication_bloc.dart';
import '../blocs/people/people_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PeopleBloc, PeopleState>(
      builder: (context, state) {
        if (state is PeopleLoadInProgress) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is PeopleLoadFailure) {
          return RefreshIndicator(
            onRefresh: () async {
              context.read<PeopleBloc>().add(PeopleLoaded(
                  BlocProvider.of<AuthenticationBloc>(context).state.user.id ??
                      0));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Center(
                  child: Text('Failed to fetch data. Please pull to refresh'),
                ),
                ElevatedButton(
                  onPressed: () => context.read<PeopleBloc>().add(PeopleLoaded(
                      BlocProvider.of<AuthenticationBloc>(context)
                              .state
                              .user
                              .id ??
                          0)),
                  child: const Text('Refresh'),
                )
              ],
            ),
          );
        }
        if (state is PeopleLoadSuccess) {
          return RefreshIndicator(
            onRefresh: () async {
              context.read<PeopleBloc>().add(PeopleLoaded(
                  BlocProvider.of<AuthenticationBloc>(context).state.user.id ??
                      0));
            },
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: TextButton(
                          onPressed: () => context
                              .read<PeopleBloc>()
                              .add(SortDirectionChanged('ASC')),
                          child: Text('Sort by Ascending')),
                    ),
                    Expanded(
                      child: TextButton(
                          onPressed: () => context
                              .read<PeopleBloc>()
                              .add(SortDirectionChanged('DESC')),
                          child: Text('Sort by Descending')),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: TextButton(
                          onPressed: () => context
                              .read<PeopleBloc>()
                              .add(ViewModeChanged('List View')),
                          child: Text('List View')),
                    ),
                    Expanded(
                      child: TextButton(
                          onPressed: () => context
                              .read<PeopleBloc>()
                              .add(ViewModeChanged('Grid View')),
                          child: Text('Grid View')),
                    ),
                  ],
                ),
                Expanded(
                  child: state.viewMode == 'List View'
                      ? ListView(
                          children: state.peoples
                              .map(
                                (people) => Container(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(child: Text(people.name)),
                                      Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              PeopleActionButtons(
                                                people: people,
                                                onFavourite: () => context
                                                    .read<PeopleBloc>()
                                                    .add(PeopleFavouriteToggled(
                                                        people)),
                                                onEdit: (String name) {
                                                  BlocProvider.of<PeopleBloc>(
                                                          context)
                                                      .add(
                                                    PeopleUpdated(
                                                      people.copyWith(
                                                        name: name,
                                                      ),
                                                    ),
                                                  );
                                                },
                                                onDelete: () => context
                                                    .read<PeopleBloc>()
                                                    .add(PeopleDeleted(people)),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              )
                              .toList(),
                        )
                      : GridView.count(
                          crossAxisCount: 2,
                          children: state.peoples
                              .map((people) => Container(
                                    padding: EdgeInsets.all(16.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          people.name,
                                          style: TextStyle(fontSize: 24.0),
                                        ),
                                        PeopleActionButtons(
                                          people: people,
                                          onFavourite: () => context
                                              .read<PeopleBloc>()
                                              .add(PeopleFavouriteToggled(
                                                  people)),
                                          onEdit: (String name) {
                                            BlocProvider.of<PeopleBloc>(context)
                                                .add(
                                              PeopleUpdated(
                                                people.copyWith(
                                                  name: name,
                                                ),
                                              ),
                                            );
                                          },
                                          onDelete: () => context
                                              .read<PeopleBloc>()
                                              .add(PeopleDeleted(people)),
                                        ),
                                      ],
                                    ),
                                  ))
                              .toList(),
                        ),
                ),
              ],
            ),
          );
        }
        return Container();
      },
    );
  }
}
