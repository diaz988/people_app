import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:people_app/blocs/authentication/authentication_bloc.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Text('Username'),
          subtitle: Text(BlocProvider.of<AuthenticationBloc>(context)
                  .state
                  .user
                  .username ??
              '-'),
        ),
        ListTile(
          title: Text('Password'),
          subtitle: Text(BlocProvider.of<AuthenticationBloc>(context)
                  .state
                  .user
                  .password ??
              '-'),
        ),
      ],
    );
  }
}
