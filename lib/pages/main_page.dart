import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:people_app/blocs/people/people.dart';
import 'package:people_app/models/models.dart';

import '../../blocs/tab/tab_bloc.dart';
import '../../models/app_tab.dart';
import '../blocs/authentication/authentication.dart';
import '../widgets/tab_selector.dart';
import 'add_edit_people_page.dart';
import 'favourite_page.dart';
import 'home_page.dart';
import 'profile_page.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => const MainPage());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TabBloc, AppTab>(
      builder: (context, activeTab) {
        return Scaffold(
          appBar: AppBar(
            title: Text('People App'),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    context
                        .read<AuthenticationBloc>()
                        .add(AuthenticationLogoutRequested());
                  },
                  child: Text(
                      'Hi ${BlocProvider.of<AuthenticationBloc>(context).state.user.username ?? '-'}, Log out')),
            ],
          ),
          body: activeTab == AppTab.HOME
              ? const HomePage()
              : activeTab == AppTab.FAVOURITE
                  ? const FavouritePage()
                  : const ProfilePage(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                AddEditPeoplePage.route(
                  onSave: (String name) {
                    BlocProvider.of<PeopleBloc>(context).add(
                      PeopleAdded(
                        People(
                          name: name,
                          userId: BlocProvider.of<AuthenticationBloc>(context)
                              .state
                              .user
                              .id,
                        ),
                      ),
                    );
                  },
                  isEditing: false,
                ),
              );
            },
            child: const Icon(Icons.add),
          ),
          bottomNavigationBar: TabSelector(
            activeTab: activeTab,
            onTabSelected: (tab) {
              if (tab == AppTab.FAVOURITE) {
                BlocProvider.of<PeopleBloc>(context).add(FavouritePeopleLoaded(
                    BlocProvider.of<AuthenticationBloc>(context)
                            .state
                            .user
                            .id ??
                        0));
              } else if (tab == AppTab.HOME) {
                BlocProvider.of<PeopleBloc>(context).add(PeopleLoaded(
                    BlocProvider.of<AuthenticationBloc>(context)
                            .state
                            .user
                            .id ??
                        0));
              }
              BlocProvider.of<TabBloc>(context).add(TabUpdated(tab));
            },
          ),
        );
      },
    );
  }
}
