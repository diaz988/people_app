import 'package:flutter/material.dart';
import '../pages/add_edit_people_page.dart';
import '../models/people.dart';

class PeopleActionButtons extends StatelessWidget {
  final Function onFavourite;
  final Function onEdit;
  final Function onDelete;
  final People people;

  PeopleActionButtons({
    Key? key,
    required this.people,
    required this.onFavourite,
    required this.onEdit,
    required this.onDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          onPressed: () => onFavourite(),
          icon: Icon(
            Icons.star,
            color: people.isFavourite == 1 ? Colors.orange : Colors.grey,
          ),
        ),
        IconButton(
          onPressed: () {
            Navigator.push(
              context,
              AddEditPeoplePage.route(
                onSave: (String name) => onEdit(name),
                isEditing: true,
                people: people,
              ),
            );
          },
          icon: const Icon(
            Icons.edit,
            color: Colors.blue,
          ),
        ),
        IconButton(
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Confirmation'),
                  content: const Text('Delete this data?'),
                  actions: [
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                        onDelete();
                      },
                      child: const Text('Yes'),
                    ),
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('No'),
                    ),
                  ],
                );
              },
            );
          },
          icon: const Icon(
            Icons.delete,
            color: Colors.red,
          ),
        ),
      ],
    );
  }
}
