import 'package:flutter/material.dart';
import 'package:people_app/app.dart';

import 'providers/api/people_api_provider.dart';
import 'providers/sqlite/people_sqlite_provider.dart';
import 'repositories/authentication_repository.dart';
import 'repositories/people_repository.dart';
import 'repositories/user_repository.dart';

void main() {
  runApp(App(
    authenticationRepository: AuthenticationRepository(),
    userRepository: UserRepository(),
    peopleRepository: PeopleRepository(
      peopleApiProvider: PeopleApiProvider(),
      peopleSqliteProvider: PeopleSqliteProvider(),
    ),
  ));
}
