import 'package:people_app/models/models.dart';
import 'package:people_app/providers/api/people_api_provider.dart';
import 'package:people_app/providers/sqlite/people_sqlite_provider.dart';

class PeopleRepository {
  final PeopleApiProvider _peopleApiProvider;
  final PeopleSqliteProvider _peopleSqliteProvider;

  PeopleRepository(
      {required PeopleApiProvider peopleApiProvider,
      required PeopleSqliteProvider peopleSqliteProvider})
      : _peopleApiProvider = peopleApiProvider,
        _peopleSqliteProvider = peopleSqliteProvider;

  Future<void> getInitialData(int userId) async {
    var peoples = await _peopleApiProvider.getInitialData(userId);
    for (var people in peoples) {
      _peopleSqliteProvider.insert(people);
    }
  }

  Future<List<People>> getPeoples(String sortDirection, int userId) async {
    return await _peopleSqliteProvider.getAllPeople(sortDirection, userId);
  }

  Future<List<People>> getFavouritePeoples(int userId) async {
    return await _peopleSqliteProvider.getFavouritePeoples(userId);
  }

  Future<People> save(People people) async {
    return await _peopleSqliteProvider.insert(people);
  }

  Future<void> delete(People people) async {
    _peopleSqliteProvider.delete(people);
  }

  Future<void> deleteAll() async {
    _peopleSqliteProvider.deleteAll();
  }

  Future<void> update(People people) async {
    _peopleSqliteProvider.update(people);
  }
}
