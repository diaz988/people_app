import 'package:people_app/models/user.dart';

class UserRepository {
  User? _user;

  Future<User?> getUser(String columnName, dynamic value) async {
    if (_user != null) {
      return _user;
    }

    return Future.delayed(
      const Duration(milliseconds: 300),
      () => _user = User(username: 'tes', password: 'tes'),
    );
  }
}
