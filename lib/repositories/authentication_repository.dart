import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/user.dart';
import '../../providers/sqlite/user_sqlite_provider.dart';

class AuthenticationRepository {
  final _user = BehaviorSubject<User>();

  Stream<User> get user => _user.stream;

  Future<bool> logIn({
    required String username,
    required String password,
  }) async {
    final userProvider = UserSqliteProvider();
    var user = await userProvider.login(username, password);
    if (user != null) {
      await doLogin(user);
    }
    return user != null;
  }

  Future<void> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int? userId = prefs.getInt('loggedInUserId');

    if (userId != null) {
      final userProvider = UserSqliteProvider();
      var user = await userProvider.getUser('_id', userId);
      if (user != null) {
        doLogin(user);
      }
    } else {
      _user.add(User());
    }
  }

  Future<bool> register({
    required String username,
    required String password,
  }) async {
    final userProvider = UserSqliteProvider();
    var user = await userProvider.getUser('username', username);
    if (user != null) {
      return false;
    }
    user = await userProvider
        .insert(User.fromMap({'username': username, 'password': password}));
    await doLogin(user);
    return true;
  }

  Future<void> doLogin(User user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('loggedInUserId', user.id!);
    _user.add(
        User(id: user.id, username: user.username, password: user.password));
  }

  void logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('loggedInUserId', -1);
    _user.add(User());
  }

  void dispose() => _user.close();
}
