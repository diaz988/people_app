import 'dart:convert';

class People {
  People({
    this.id,
    required this.name,
    this.isFavourite = 0,
    this.userId,
  });

  final int? id;
  final String name;
  final int? isFavourite;
  final int? userId;

  People copyWith({
    final int? id,
    String? name,
    List<String>? films,
    List<String>? species,
    DateTime? created,
    DateTime? edited,
    int? isFavourite,
    int? userId,
  }) =>
      People(
        id: id ?? this.id,
        name: name ?? this.name,
        isFavourite: isFavourite ?? this.isFavourite,
        userId: userId ?? this.userId,
      );

  factory People.fromJson(String str) => People.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory People.fromMap(Map<String, dynamic> json) => People(
        id: json["_id"],
        name: json["name"],
        isFavourite: json["is_favourite"],
        userId: json["user_id"],
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "name": name,
      "is_favourite": isFavourite,
      "user_id": userId
    };
    if (id != null) {
      map["_id"] = id;
    }
    return map;
  }
}
