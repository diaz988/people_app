import 'dart:convert';

import 'people.dart';

class PeopleResponse {
  PeopleResponse({
    required this.count,
    required this.next,
    required this.previous,
    required this.results,
  });

  final int count;
  final String next;
  final dynamic previous;
  final List<People> results;

  factory PeopleResponse.fromJson(String str) => PeopleResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PeopleResponse.fromMap(Map<String, dynamic> json) => PeopleResponse(
    count: json["count"],
    next: json["next"],
    previous: json["previous"],
    results: List<People>.from(json["results"].map((x) => People.fromMap(x))),
  );

  Map<String, dynamic> toMap() => {
    "count": count,
    "next": next,
    "previous": previous,
    "results": List<dynamic>.from(results.map((x) => x.toMap())),
  };
}

