import 'dart:convert';

import 'package:equatable/equatable.dart';

class User extends Equatable {
  const User({
    this.id,
    this.username,
    this.password,
  });

  final int? id;
  final String? username;
  final String? password;

  factory User.fromJson(String str) => User.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory User.fromMap(Map<String, dynamic> json) => User(
        id: json["_id"],
        username: json["username"],
        password: json["password"],
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "username": username,
      "password": password,
    };
    if (id != null) {
      map["_id"] = id;
    }
    return map;
  }

  User copyWith({
    final int? id,
    final String? username,
    final String? password,
  }) =>
      User(
          id: id ?? this.id,
          username: username ?? this.username,
          password: password ?? this.password);

  @override
  List<Object?> get props => [username, password];

  static const empty = User();
}
