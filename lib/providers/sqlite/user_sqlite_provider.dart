import 'package:sqflite/sqflite.dart';

import '../../../models/user.dart';
import 'db_provider.dart';

const String tableUser = 'users';
const String columnId = '_id';
const String columnUsername = 'username';
const String columnPassword = 'password';

class UserSqliteProvider {
  Database? db;

  Future open() async {
    db = await DBProvider.db.database;
  }

  Future<User> insert(User user) async {
    await open();
    int id = await db!.insert(tableUser, user.toMap());

    return user.copyWith(id: id);
  }

  Future<User?> getUser(String columnName, dynamic value) async {
    await open();
    List<Map<String, dynamic>> maps = await db!.query(tableUser,
        columns: [columnId, columnPassword, columnUsername],
        where: '$columnName = ?',
        whereArgs: [value]);
    if (maps.isNotEmpty) {
      return User.fromMap(maps.first);
    }
    return null;
  }

  Future<User?> login(String username, String password) async {
    await open();
    List<Map<String, dynamic>> maps = await db!.query(tableUser,
        columns: [columnId, columnPassword, columnUsername],
        where: '$columnUsername = ? AND $columnPassword = ?',
        whereArgs: [username, password]);
    if (maps.isNotEmpty) {
      return User.fromMap(maps.first);
    }
    return null;
  }

  Future<int> delete(int id) async {
    await open();
    return await db!.delete(tableUser, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> update(User user) async {
    await open();
    return await db!.update(tableUser, user.toMap(),
        where: '$columnId = ?', whereArgs: [user.id]);
  }

  Future close() async => db!.close();
}
