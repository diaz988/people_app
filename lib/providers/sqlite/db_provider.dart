import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  static Database? _database;

  Future<Database?> get database async {
    if (_database != null) {
      return _database;
    }

    _database = await initDB();
    return _database;
  }

  static const tablePeoples = """
  create table peoples ( 
  _id integer primary key autoincrement, 
  name text not null,
  user_id integer not null,
  is_favourite integer not null
      );""";

  static const tableUsers = """
  create table users ( 
  _id integer primary key autoincrement, 
  username text not null,
  password text not null
      );""";

  Future<Database> initDB() async {
    return await openDatabase('my_db.db', version: 3,
        onCreate: (Database db, int version) async {
      await db.execute(tableUsers);
      await db.execute(tablePeoples);
    });
  }
}
