import 'package:people_app/providers/sqlite/db_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../../../models/people.dart';
import 'db_provider.dart';

const String tablePeople = 'peoples';
const String columnId = '_id';
const String columnName = 'name';
const String columnUserId = 'user_id';
const String columnIsFavourite = 'is_favourite';

class PeopleSqliteProvider {
  Database? db;

  Future open() async {
    db = await DBProvider.db.database;
  }

  Future<People> insert(People people) async {
    await open();
    int id = await db!.insert(tablePeople,
        {'name': people.name, 'user_id': people.userId, 'is_favourite': 0});
    return people.copyWith(id: id);
  }

  Future<People?> getPeople(String columnName, dynamic value) async {
    await open();
    List<Map<String, dynamic>> maps = await db!.query(tablePeople,
        columns: [columnId, columnName],
        where: '$columnName = ?',
        whereArgs: [value]);
    if (maps.isNotEmpty) {
      return People.fromMap(maps.first);
    }
    return null;
  }

  Future<List<People>> getAllPeople(String sortDirection, int userId) async {
    await open();
    List<Map<String, dynamic>> maps = await db!.query(
      tablePeople,
      columns: [columnId, columnName, columnIsFavourite, columnUserId],
      orderBy: "$columnName $sortDirection",
      where: 'user_id = ?',
      whereArgs: [userId],
    );
    if (maps.isNotEmpty) {
      return (maps.map((peopleMap) => People.fromMap(peopleMap))).toList();
    }
    return [];
  }

  Future<List<People>> getFavouritePeoples(int userId) async {
    await open();
    List<Map<String, dynamic>> maps = await db!.query(
      tablePeople,
      columns: [columnId, columnName, columnIsFavourite, columnUserId],
      where: 'is_favourite = 1 AND user_id = ?',
      whereArgs: [userId],
    );
    if (maps.isNotEmpty) {
      return (maps.map((peopleMap) => People.fromMap(peopleMap))).toList();
    }
    return [];
  }

  Future<int> delete(People people) async {
    return await db!
        .delete(tablePeople, where: '$columnId = ?', whereArgs: [people.id]);
  }

  Future<void> deleteAll() async {
    await db!.execute("delete from $tablePeople");
  }

  Future<int> update(People people) async {
    return await db!.update(
        tablePeople, {'name': people.name, 'is_favourite': people.isFavourite},
        where: '$columnId = ?', whereArgs: [people.id]);
  }

  Future close() async => db!.close();
}
