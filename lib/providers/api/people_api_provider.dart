import 'dart:convert';
import 'dart:io';

import '../../models/people.dart';

class PeopleApiProvider {
  Future<List<People>> getInitialData(int userId) async {
    // The root certificate is expired, so we ignore the certificate
    HttpClient client = HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    var url = Uri.parse('https://swapi.dev/api/people/?format=json');
    HttpClientRequest request = await client.getUrl(url);
    HttpClientResponse result = await request.close();
    final data = jsonDecode(await result.transform(utf8.decoder).join());

    var peoples = List<People>.from(data['results'].map((e) {
      var people = People.fromMap({
        'name': e['name'],
        'is_favourite': 0,
        'user_id': userId,
      });
      return people;
    }).toList());

    return peoples;
  }
}
