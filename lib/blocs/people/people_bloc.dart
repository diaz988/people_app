import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/people.dart';
import '../../models/user.dart';
import '../../repositories/authentication_repository.dart';
import '../../repositories/people_repository.dart';

part 'people_event.dart';

part 'people_state.dart';

class PeopleBloc extends Bloc<PeopleEvent, PeopleState> {
  final PeopleRepository peopleRepository;
  final AuthenticationRepository authenticationRepository;

  PeopleBloc(
      {required this.peopleRepository, required this.authenticationRepository})
      : super(PeopleLoadInProgress()) {
    _authenticationStatusSubscription = authenticationRepository.user.listen(
      (user) => add(PeopleLoaded(user.id ?? 0)),
    );
  }

  late StreamSubscription<User?> _authenticationStatusSubscription;

  @override
  Stream<PeopleState> mapEventToState(PeopleEvent event) async* {
    if (event is PeopleLoaded) {
      yield* _mapPeopleLoadedToState(event);
    } else if (event is FavouritePeopleLoaded) {
      yield* _mapFavouritePeopleLoadedToState(event);
    } else if (event is PeopleAdded) {
      yield* _mapPeopleAddedToState(event);
    } else if (event is ViewModeChanged) {
      yield* _mapViewModeChangedToState(event);
    } else if (event is SortDirectionChanged) {
      yield* _mapSortDirectionChangedToState(event);
    } else if (event is PeopleUpdated) {
      yield* _mapPeopleUpdatedToState(event);
    } else if (event is PeopleDeleted) {
      yield* _mapPeopleDeletedToState(event);
    } else if (event is PeopleFavouriteToggled) {
      yield* _mapPeopleFavouriteToggledToState(event);
    }
  }

  Stream<PeopleState> _mapPeopleLoadedToState(PeopleLoaded event) async* {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? isFirstTimeLogin = prefs.getBool('isFirstTimeLogin');
    if (isFirstTimeLogin != null) {
      await peopleRepository.deleteAll();
      await peopleRepository.getInitialData(event.userId);
      prefs.remove('isFirstTimeLogin');
    }
    try {
      final peoples = await peopleRepository.getPeoples('ASC', event.userId);
      yield PeopleLoadSuccess(userId: event.userId,
          isFavouriteOnly: false, peoples: peoples, viewMode: 'List View');
    } catch (e) {
      yield PeopleLoadFailure();
    }
  }

  Stream<PeopleState> _mapFavouritePeopleLoadedToState(FavouritePeopleLoaded event) async* {
    try {
      final peoples = await peopleRepository.getFavouritePeoples(event.userId);
      yield PeopleLoadSuccess(userId: event.userId,
          isFavouriteOnly: true, peoples: peoples, viewMode: 'List View');
    } catch (e) {
      yield PeopleLoadFailure();
    }
  }

  Stream<PeopleState> _mapPeopleAddedToState(PeopleAdded event) async* {
    if (state is PeopleLoadSuccess) {
      People people = await peopleRepository.save(event.people);
      final List<People> updatedPeoples =
          List.from((state as PeopleLoadSuccess).peoples)..add(people);
      yield (state as PeopleLoadSuccess).copyWith(peoples: updatedPeoples);
    }
  }

  Stream<PeopleState> _mapPeopleUpdatedToState(PeopleUpdated event) async* {
    if (state is PeopleLoadSuccess) {
      final List<People> updatedPeoples =
          (state as PeopleLoadSuccess).peoples.map((people) {
        return people.id == event.people.id ? event.people : people;
      }).toList();
      yield (state as PeopleLoadSuccess).copyWith(peoples: updatedPeoples);
      peopleRepository.update(event.people);
    }
  }

  Stream<PeopleState> _mapPeopleFavouriteToggledToState(
      PeopleFavouriteToggled event) async* {
    if (state is PeopleLoadSuccess) {
      final People updatedPeople = event.people
          .copyWith(isFavourite: event.people.isFavourite == 1 ? 0 : 1);
      final List<People> updatedPeoples;
      if ((state as PeopleLoadSuccess).isFavouriteOnly &&
          updatedPeople.isFavourite == 0) {
        updatedPeoples = (state as PeopleLoadSuccess).peoples.where((people) {
          return people.id != updatedPeople.id;
        }).toList();
      } else {
        updatedPeoples = List<People>.from((state as PeopleLoadSuccess).peoples)
            .map((people) {
          return people.id == updatedPeople.id ? updatedPeople : people;
        }).toList();
      }
      yield (state as PeopleLoadSuccess).copyWith(peoples: updatedPeoples);
      peopleRepository.update(updatedPeople);
    }
  }

  Stream<PeopleState> _mapPeopleDeletedToState(PeopleDeleted event) async* {
    if (state is PeopleLoadSuccess) {
      final updatedPeoples = (state as PeopleLoadSuccess)
          .peoples
          .where((people) => people.id != event.people.id)
          .toList();
      yield (state as PeopleLoadSuccess).copyWith(peoples: updatedPeoples);
      peopleRepository.delete(event.people);
    }
  }

  Stream<PeopleState> _mapViewModeChangedToState(ViewModeChanged event) async* {
    if (state is PeopleLoadSuccess) {
      yield (state as PeopleLoadSuccess).copyWith(viewMode: event.viewMode);
    }
  }

  Stream<PeopleState> _mapSortDirectionChangedToState(
      SortDirectionChanged event) async* {
    if (state is PeopleLoadSuccess) {
      final peoples = await peopleRepository.getPeoples(event.sortDirection, (state as PeopleLoadSuccess).userId);
      yield (state as PeopleLoadSuccess)
          .copyWith(sortDirection: event.sortDirection, peoples: peoples);
    }
  }
}
