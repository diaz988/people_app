part of 'people_bloc.dart';

abstract class PeopleState extends Equatable {
  const PeopleState();

  @override
  List<Object> get props => [];
}

class PeopleLoadInProgress extends PeopleState {}

class PeopleLoadSuccess extends PeopleState implements EquatableMixin {
  final List<People> peoples;
  final bool isFavouriteOnly;
  final String viewMode;
  final int userId;

  const PeopleLoadSuccess(
      {required this.userId,
      required this.isFavouriteOnly,
      required this.peoples,
      required this.viewMode})
      : super();

  PeopleLoadSuccess copyWith({
    int? userId,
    List<People>? peoples,
    bool? isFavouriteOnly,
    String? viewMode,
    String? sortDirection,
  }) {
    return PeopleLoadSuccess(
      userId: userId ?? this.userId,
      peoples: peoples ?? this.peoples,
      isFavouriteOnly: isFavouriteOnly ?? this.isFavouriteOnly,
      viewMode: viewMode ?? this.viewMode,
    );
  }

  @override
  List<Object> get props => [userId, isFavouriteOnly, peoples, viewMode];

  @override
  String toString() => 'PeopleLoadSuccess { peoples: $peoples }';
}

class PeopleLoadFailure extends PeopleState {}
