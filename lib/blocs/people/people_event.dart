part of 'people_bloc.dart';

abstract class PeopleEvent extends Equatable {
  const PeopleEvent();

  @override
  List<Object> get props => [];
}

class PeopleLoaded extends PeopleEvent {
  final int userId;

  const PeopleLoaded(this.userId);

  @override
  List<Object> get props => [userId];
}

class FavouritePeopleLoaded extends PeopleEvent {
  final int userId;

  const FavouritePeopleLoaded(this.userId);

  @override
  List<Object> get props => [userId];
}

class ViewModeChanged extends PeopleEvent {
  final String viewMode;

  const ViewModeChanged(this.viewMode);

  @override
  List<Object> get props => [viewMode];
}

class SortDirectionChanged extends PeopleEvent {
  final String sortDirection;

  const SortDirectionChanged(this.sortDirection);

  @override
  List<Object> get props => [sortDirection];
}

class PeopleAdded extends PeopleEvent {
  final People people;

  const PeopleAdded(this.people);

  @override
  List<Object> get props => [people];

  @override
  String toString() => 'PeopleAdded { people: $people }';
}

class PeopleFavouriteToggled extends PeopleEvent {
  final People people;

  const PeopleFavouriteToggled(this.people);

  @override
  List<Object> get props => [people];

  @override
  String toString() => 'PeopleAdded { people: $people }';
}

class PeopleUpdated extends PeopleEvent {
  final People people;

  const PeopleUpdated(this.people);

  @override
  List<Object> get props => [people];

  @override
  String toString() => 'PeopleUpdated { people: $people }';
}

class PeopleDeleted extends PeopleEvent {
  final People people;

  const PeopleDeleted(this.people);

  @override
  List<Object> get props => [people];

  @override
  String toString() => 'PeopleDeleted { people: $people }';
}

class ToggleFavourite extends PeopleEvent {
  final People people;

  const ToggleFavourite(this.people);

  @override
  List<Object> get props => [people];

  @override
  String toString() => 'PeopleDeleted { people: $people }';
}
