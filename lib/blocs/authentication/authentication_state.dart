part of 'authentication_bloc.dart';

class AuthenticationState extends Equatable {
  AuthenticationState._({
    this.user = User.empty,
  });

   AuthenticationState.authenticated(User user) : this._(user: user);

   AuthenticationState.unauthenticated() : this._();

  final User user;

  @override
  List<Object> get props => [user];
}
